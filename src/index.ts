// const arr: number[] = [2, 3, 4, 5, 6, 7, 8, 9];

// for (let a in arr) {
//     console.log(arr[a]);
// }
class PPP {
    private fifo: number[] = [];

    public async getFromFifo(numbers: number[]): Promise<number> {
        const promise = new Promise<number>((resolve) => {
            setTimeout(() => {
                const firstOut: number = numbers.shift() ?? 0;
                console.log('firstOut', firstOut);
                return resolve(firstOut);
            }, 2000);
        });

        return promise;
    }

    public async doasync(): Promise<void> {

        this.fifo.push(1);
        this.fifo.push(2);
        this.fifo.push(3);
        this.fifo.push(4);
        this.fifo.push(5);

        console.log('this.fifo', this.fifo);

        await this.getFromFifo(this.fifo);
        console.log('this.fifo', this.fifo);

        await this.getFromFifo(this.fifo);
        console.log('this.fifo', this.fifo);

        await this.getFromFifo(this.fifo);
        console.log('this.fifo', this.fifo);

        await this.getFromFifo(this.fifo);
        console.log('this.fifo', this.fifo);

        await this.getFromFifo(this.fifo);
        console.log('this.fifo', this.fifo);

        await this.getFromFifo(this.fifo);
        console.log('this.fifo', this.fifo);

        // this.fifo.push(1);
        // this.fifo.push(2);
        // this.fifo.push(3);

        // const result = await this.getFromFifo(this.fifo);
        // console.log(result); // --> 'done!';
        // return result;
    }
}

const ppp: PPP = new PPP();
console.log('1111');
ppp.doasync();
console.log('2222');
