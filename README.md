## Install

Clone the repository and install packages:
`npm install`

## Start

Just type command with the name of file to play:  
`npm run start`
